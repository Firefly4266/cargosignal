import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ImatwitterComponent } from './imatwitter/imatwitter.component';

@NgModule({
  declarations: [
    AppComponent,
    ImatwitterComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
