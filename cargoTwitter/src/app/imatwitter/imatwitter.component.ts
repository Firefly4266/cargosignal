import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-imatwitter',
  templateUrl: './imatwitter.component.html',
  styleUrls: ['./imatwitter.component.css']
})
export class ImatwitterComponent implements OnInit {
  @Output() newTwit = new EventEmitter<string>();
  submitIt(twit: string) {
  this.newTwit.emit(twit);
 }

  constructor() { }

  ngOnInit() {
  }

}
