import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImatwitterComponent } from './imatwitter.component';

describe('ImatwitterComponent', () => {
  let component: ImatwitterComponent;
  let fixture: ComponentFixture<ImatwitterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImatwitterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImatwitterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
